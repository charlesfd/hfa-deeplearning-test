# Deep learning test

## Problème

Développer un modèle capable d'identifier le locuteur (Chirac vs. Mitterrand) d'un segment de discours politique.

Un dataset labellisé est fourni pour l'apprentissage

## Data

Les données sont au format Pickle

Les `sequences` sont la version preprocessed des `sentences`. Le dictionnaire produit lors de la tokenization est fourni.

Les données de `Test/` ne sont pas labellisées

## Objectif

Prédire les labels de  `Test` (en respectant le format utilisé dans `Learn`)

## Points clés

- Qualité / clarté du code produit
- Simplicité de la solution
- L'utilisation d'un modèle deep learning est un plus

## Format attendu du travail effectué :

- Rendu détaillé du travail effectué et de la démarche entreprise, l'objectif étant de valider la capacité du candidat à expliquer à des non-techniques les démarches entreprises et les moyens exploités pour obtenir les résultats.
- D'un point de vu technique ce qui va être, essentiellement, évaluer c'est l'intelligence des démarches entreprises et les compétences en développement.
- D'une manière globale le test à aussi pour but de vérifier les éléments clefs du poste.

Le résultat des prédictions compte dans une moindre mesure
